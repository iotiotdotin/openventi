import React from 'react';
import Iframe from 'react-iframe';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

import Image from '@theme/IdealImage';




function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  

  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Contributors<head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Our Contributors</h1>
          <p className="hero__subtitle">Thanks to everyone for their contribution.</p>
          <p className="hero__subtitle">This Project is thriving due to contributions made by these people.</p>
        </div>
      </header>
      <main>
        <iframe src="http://3.7.55.159:3003/"></iframe>
      </main>
    </Layout>
  );
}

export default Home;
